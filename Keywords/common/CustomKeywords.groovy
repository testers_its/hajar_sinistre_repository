package common

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory



public class CustomKeywords {


	@Keyword
	def DynamicSendKeys(String PropertyName,String InputText){
		WebDriver driver = DriverFactory.getWebDriver()

		WebDriverWait wait = new WebDriverWait(driver, 10)
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PropertyName)))

		driver.findElement(By.xpath(PropertyName)).sendKeys(InputText)

	}

	@Keyword
	def DynamicGetByAtttribute(String PropertyName,String Atttributename){
		WebDriver driver = DriverFactory.getWebDriver()

		WebDriverWait wait = new WebDriverWait(driver, 10)
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PropertyName)))

		String GetText=driver.findElement(By.xpath(PropertyName)).getAttribute(Atttributename)

		return GetText

	}


	@Keyword
	def CheckisEnabled(TestObject testObj,String attribute){
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 10)
		try{
			
			
			//String Propertyvalue =WebUI.getAttribute(findTestObject(testObj.objectId), attribute)
			
			String Propertyvalue = findTestObject(testObj.objectId).findPropertyValue('xpath', false)
			boolean flagEnabled=driver.findElement(By.xpath(Propertyvalue)).isEnabled()
			return flagEnabled
		}
		catch(Exception e){
			KeywordUtil.markFailed('There is an error in executing keyword:-'+e.getMessage())
		}


	}
	
	
	@Keyword
	def GetTextByAttrb(TestObject testObj,String attribute){
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 10)
		try{
							
			String Propertyvalue = findTestObject(testObj.objectId).findPropertyValue('xpath', false)
			driver.findElement(By.xpath(Propertyvalue)).click()
			def controlValue=driver.findElement(By.xpath(Propertyvalue)).getAttribute("value")
			return controlValue
		}
		catch(Exception e){
			KeywordUtil.markFailed('There is an error in executing keyword:-'+e.getMessage())
		}


	}
	

}
