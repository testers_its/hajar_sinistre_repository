package common

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.WebDriverWait

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import org.openqa.selenium.JavascriptExecutor

public class ExplicitWaits {

	static KeywordLogger log = new KeywordLogger()

	@Keyword
	public static void numberOfElementsToBeMoreThan(TestObject testObj,int noofRecords,int waitForsec){

		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebDriverWait wait = new WebDriverWait(driver, waitForsec)
			String FactOrgPropValue = findTestObject(testObj.objectId).findPropertyValue("xpath", false)
			wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(FactOrgPropValue), noofRecords))
			log.logPassed("The Requested condition matched for given state")
			KeywordUtil.markPassed("The Requested condition matched for given state")
		} catch(Exception e){
			log.logFailed("There is exception in executing the Keyword -numberOfElementsToBeMoreThan[with 500 MILLISECONDS interva]-"+e.getMessage())
			KeywordUtil.markFailedAndStop("There is exception in executing the Keyword -numberOfElementsToBeMoreThan[with 500 MILLISECONDS interva]-"+e.getMessage())
		}
	}
	
	
	@Keyword
	public static void textToBePresentInElementLocated(TestObject testObj,String matchText,int waitForsec){

		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebDriverWait wait = new WebDriverWait(driver, waitForsec)
			String FactOrgPropValue = findTestObject(testObj.objectId).findPropertyValue("xpath", false)
			wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(FactOrgPropValue),matchText))
			log.logPassed("The Requested condition matched for given state")
			KeywordUtil.markPassed("The Requested condition matched for given state")
		} catch(Exception e){
			log.logFailed("There is exception in executing the Keyword -textToBePresentInElementLocated[with 500 MILLISECONDS interva]-"+e.getMessage())
			KeywordUtil.markFailedAndStop("There is exception in executing the Keyword -textToBePresentInElementLocated[with 500 MILLISECONDS interva]-"+e.getMessage())
		}
	}
	
	
	@Keyword
	public static void textToBePresentInElementValue(TestObject testObj,String matchText,int waitForsec){

		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebDriverWait wait = new WebDriverWait(driver, waitForsec)
			String FactOrgPropValue = findTestObject(testObj.objectId).findPropertyValue("xpath", false)
			wait.until(ExpectedConditions.textToBePresentInElementValue(By.xpath(FactOrgPropValue), matchText))
			log.logPassed("The Requested condition matched for given state")
			KeywordUtil.markPassed("The Requested condition matched for given state")
		} catch(Exception e){
			log.logFailed("There is exception in executing the Keyword -textToBePresentInElementValue[with 500 MILLISECONDS interva]-"+e.getMessage())
			KeywordUtil.markFailedAndStop("There is exception in executing the Keyword -textToBePresentInElementValue[with 500 MILLISECONDS interva]-"+e.getMessage())
		}
	}
	
/*	@Keyword
	public static void ajaxCallFinish(){
		WebDriver driver = DriverFactory.getWebDriver();
		JavascriptExecutor executor = ((JavascriptExecutor)driver);
		//WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
		WebDriverWait wait = new WebDriverWait(driver, 30)
	//	IJavaScriptExecutor jsScript = Driver as IJavaScriptExecutor;
	//	wait.Until((d) => (bool)executor.ExecuteScript("return jQuery.active == 0"));
		wait.until(executor.executeScript("return jQuery.active == 0"))
	}
	*/


}
