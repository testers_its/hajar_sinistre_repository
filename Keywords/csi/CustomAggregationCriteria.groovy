package csi

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable
import org.openqa.selenium.By as By
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.support.ui.ExpectedConditions
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

public class CustomAgregationCriteria {

	static WebDriver driver = DriverFactory.getWebDriver()
	static WebDriverWait wait = new WebDriverWait(driver, 10)
	static WebElement element
	static Select oSelect


	@Keyword
	public static void Category(TestObject objectto,String InputValue){

		try{


			def Category="//app-aggregation-add-edit/div/div[2]/div/form/fieldset/div[3]/div/select"
			def ModObject



			//Code to set value of Criteria Field
			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',Category , true);
			element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(ModObject, 20)))

			element.click()
			Select oSelect = new Select(element)
			//oSelect.selectByValue("PUB_TRANSFER")
			oSelect.selectByValue(InputValue)
			element.click()

		}

		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in exection of Keyword details:-' + e.getMessage())
		}


	}

	@Keyword
	public static void FielToAggregate(TestObject objectto,String InputValue){

		try{


			def FielToAggregate="//app-aggregation-add-edit/div/div[2]/div/form/fieldset/div[4]/div/select"
			def ModObject



			//Code to set value of Criteria Field
			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',FielToAggregate , true);
			element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(ModObject, 20)))

			element.click()
			Select oSelect = new Select(element)
			//oSelect.selectByValue("PUB_TRANSFER")
			oSelect.selectByValue(InputValue)
			element.click()

		}

		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in exection of Keyword details:-' + e.getMessage())
		}


	}



	@Keyword
	public static void CriteriaField(TestObject objectto,String InputValue){

		try{


			def CriteriaSection="//div/app-aggregation-add-edit/div/div[2]/div/form/fieldset/div[5]/div[1]/div[1]/span[1]"
			def CriteriaColumn="//app-aggregation-add-edit/div/div[2]/div/form/fieldset/div[5]/div[1]/div[2]/aggregation-criteria/div[1]/div[1]/select"


			def ModObject

			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',CriteriaSection , true);
			def mynewvalue=WebUI.getText(ModObject)

			System.out.print("*****************After replacement************************"+mynewvalue);


			//Code to set value of Criteria Field
			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',CriteriaColumn , true);
			element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(ModObject, 20)))

			element.click()
			Select oSelect = new Select(element)
			//oSelect.selectByValue("PUB_TRANSFER")
			oSelect.selectByValue(InputValue)
			element.click()

		}

		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in exection of Keyword details:-' + e.getMessage())
		}


	}

	@Keyword
	public static void CriteriaOperator(TestObject objectto,String InputValue){

		try{
			def CriteriaSection="//div/app-aggregation-add-edit/div/div[2]/div/form/fieldset/div[5]/div[1]/div[1]/span[1]"
			def CriteriaOperator="//app-aggregation-add-edit/div/div[2]/div/form/fieldset/div[5]/div[1]/div[2]/aggregation-criteria/div[1]/div[2]/select"


			def ModObject

			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',CriteriaSection , true);
			def mynewvalue=WebUI.getText(ModObject)

			System.out.print("*****************After replacement************************"+mynewvalue);


			//Code to set value of Operator Field
			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',CriteriaOperator , true);
			element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(ModObject, 20)))

			element.click()
			oSelect = new Select(element)
			oSelect.selectByValue(InputValue)
			element.click()

		}

		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in exection of Keyword details:-' + e.getMessage())
		}


	}


	@Keyword
	public static void CriteriaValue(TestObject objectto,String InputValue){

		try{
			def CriteriaSection="//div/app-aggregation-add-edit/div/div[2]/div/form/fieldset/div[5]/div[1]/div[1]/span[1]"
			def CriteriaValue="//app-aggregation-add-edit/div/div[2]/div/form/fieldset/div[5]/div[1]/div[2]/aggregation-criteria/div[1]/div[3]/input"


			def ModObject

			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',CriteriaSection , true);
			def mynewvalue=WebUI.getText(ModObject)

			System.out.print("*****************After replacement************************"+mynewvalue);

			//Code to set value of Criteria value
			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',CriteriaValue , true);
			element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(ModObject, 20)))
			element.sendKeys("10")
		}

		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in exection of Keyword details:-' + e.getMessage())
		}


	}

	@Keyword
	public static void BtnSave(TestObject objectto,String InputValue){

		try{


			def SaveXpath="//button[@type='submit']"
			def ModObject



			//Code to set value of Criteria Field
			ModObject=WebUI.modifyObjectProperty(objectto, 'xpath', 'equals',SaveXpath , true);
			element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(ModObject, 20)))
			element.click()
			

		}

		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in exection of Keyword details:-' + e.getMessage())
		}


	}


}
