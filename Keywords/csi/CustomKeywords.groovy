package csi

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.FluentWait
import org.openqa.selenium.support.ui.Wait
import org.openqa.selenium.support.ui.WebDriverWait
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.Wait;
import com.google.common.base.Function;

public class CustomKeywords {

	//Keyword added to support the set text
	@Keyword
	public static void CustomSetText(TestObject objectto, String InputText){
		WebDriver driver = DriverFactory.getWebDriver()

		WebDriverWait wait = new WebDriverWait(driver, 10)
		try{

			WebUI.delay(1)
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(objectto, 20)))
			//element.click();
			element.clear();
			element.sendKeys(InputText)

		}
		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in Executing of Keyword details:-' + e.getMessage())
		}


	}





	//Keyword added to support the Click
	@Keyword
	public static void CustomSearchSelect(TestObject ClickObj,TestObject TypeObj,TestObject SelectObj,String Inputtext,String MatchType){


		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebDriverWait wait = new WebDriverWait(driver, 10)

			WebUI.delay(1)
			
			WebElement elementClick = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(ClickObj, 5)))
			elementClick.click()
								
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(TypeObj, 5)))
			element.sendKeys(Inputtext)

			String FactOrgPropValue = findTestObject(SelectObj.objectId).findPropertyValue('xpath', false)

			List<WebElement> AllRows = driver.findElements(By.xpath(FactOrgPropValue))

			
			if(AllRows.size()>0){
				for(int i=0;i<AllRows.size();i++){
					String SelectValue=AllRows.get(i).getText()

					System.out.print("SelectValue:"+SelectValue)

					if(MatchType.toUpperCase()=="CONTAINS"){

						if(SelectValue.contains(Inputtext)){
							AllRows.get(i).click()

							//Confirm the text selected
							String SelectedText=WebUI.getText(findTestObject(ClickObj.objectId))
							if(SelectedText.contains(Inputtext)){
								KeywordUtil.markPassed("Given value Selected for:-"+Inputtext)

							}
							else{
								KeywordUtil.markFailedAndStop("Given value not Selected for:-"+Inputtext)
							}
						}
					}
					else
					{
						if(SelectValue.equalsIgnoreCase(Inputtext)){
							AllRows.get(i).click()

							//Confirm the text selected
							String SelectedText=WebUI.getText(findTestObject(ClickObj.objectId))
							if(SelectedText.contains(Inputtext)){
								KeywordUtil.markPassed("Given value Selected for:-"+Inputtext)

							}
							else{
								KeywordUtil.markFailedAndStop("Given value not Selected for:-"+Inputtext)
							}
						}
					}

				}
			}
			else{
				KeywordUtil.markFailedAndStop("Given value not found for:-"+Inputtext)
			}

		}
		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in Executing of Keyword details:-' + e.getMessage())
		}

	}


	//Keyword added to support the set text
	@Keyword
	public static void CustomSetSelect(TestObject objectto, String InputText){
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 10)
		try{

			WebUI.delay(1)
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(objectto, 20)))
			element.click();
			//element.clear();
			element.sendKeys(InputText)

		}
		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in Executing of Keyword details:-' + e.getMessage())
		}

	}


	//Keyword added to support the set text
	@Keyword
	public static void CustomSelectCondition(String InputText){
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 10)
		try{


			List<WebElement> GetListCount=driver.findElements(By.xpath("//ng-select/select-dropdown/div/div[2]/ul/li/span"))

			KeywordUtil.logInfo('Total # of matching records'+GetListCount.size())

			if (GetListCount.size()==1){

				WebElement element=driver.findElement(By.xpath("//ng-select/select-dropdown/div/div[2]/ul/li[1]/span"))
				String MySelect=element.getText()

				KeywordUtil.logInfo('Value to be selected Text '+MySelect)

				if(MySelect.equalsIgnoreCase(InputText)){

					KeywordUtil.logInfo('Value Matched as expected Clicking on the value')
					element.click()

					element=driver.findElement(By.xpath("//ng-select[@formcontrolname='LeftField']/div/div/div/span"))
					String MySelectcted=element.getText()


					if(MySelectcted.equalsIgnoreCase(InputText)){
						KeywordUtil.markPassed('Selected Value Matched as Expected Value')
					}
					else{
						KeywordUtil.markFailed('Selected Value does not matched as Expected Value')
					}

				}
				else{
					KeywordUtil.markFailed('Selected Value does not matched as Expected Value')
				}


			}
			else{
				KeywordUtil.markFailed('There is no single record for matching critria'+GetListCount.size())
			}

		}
		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in Executing of Keyword details:-' + e.getMessage())
		}

	}





}
