package csi

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import internal.GlobalVariable
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.support.ui.Select as Select
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil


public class Research {
	
	
	
	//Keyword added to support the Get Grid Count
	@Keyword
	public static void GetGridCount(TestObject objectto, int ExpectedCount){
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 10)

		try{

			WebUI.delay(1)
			List<WebElement> GridLocator = WebUiCommonHelper.findWebElements(objectto, 20)


			if(GridLocator.size().equals(ExpectedCount)){
				KeywordUtil.markPassed('Grid Record count matched Expected:'+ExpectedCount+" Actual:-"+GridLocator.size())
			}
			else
			{
				KeywordUtil.markFailedAndStop('Grid Record count does not matched Expected:'+ExpectedCount+" Actual:-"+GridLocator.size())
			}


		}
		catch(Exception e){
			KeywordUtil.markFailedAndStop('There is an exception in Executing of Keyword details:-' + e.getMessage())
		}

	}


		
	

	
}
