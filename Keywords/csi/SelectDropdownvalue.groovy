package csi

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import internal.GlobalVariable
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.support.ui.Select as Select
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

public class SelectDropdownvalue {

	
	@Keyword
	public static void CutomSelectDropdown(TestObject objectto, String InputText){
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 10)
		
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(objectto, 20)))
		element.click()
		Select oSelect = new Select(WebUiCommonHelper.findWebElement(objectto, 20))
		oSelect.selectByVisibleText(InputText)
		element.click()
		
		String selectedText=oSelect.getFirstSelectedOption().getText()
		
		if(selectedText.equalsIgnoreCase(InputText)){
			KeywordUtil.markPassed('Selection of the item is successful for:-' + InputText)
		}
		else
		{
			KeywordUtil.markFailedAndStop('Selection of the item is Failed' + InputText)
		}
	}
	
	@Keyword
	public static void WaitClickable(TestObject objectto){
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 10)

		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(WebUiCommonHelper.findWebElement(objectto, 20)))
		element.click()
	}

}
