package csi

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.FluentWait;
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.google.common.base.Function;
import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class TimeWait {

	@Keyword
	public static void Mywait(){
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
			wait.pollingEvery(1, TimeUnit.SECONDS)
			wait.withTimeout(30, TimeUnit.SECONDS)
			WebElement element = wait.until(new Function<WebDriver, WebElement>(){
				public WebElement apply(WebDriver arg0)
				{

					List<WebElement> ele = arg0.findElements(By.xpath('//ng-select[@formcontrolname="publisherNumber"]/select-dropdown/div/div[2]/ul/li/span'));

					if (ele.size()>2)
					{

						System.out.println("Value is >>> " + ele.size());

						return ele;

					}
					else {
						System.out.println("Value is >>> " + ele.size());
						return null;

					}

				}

			});
			System.out.println("Final visible status is >>>>> " + element.isDisplayed());
		}

	

catch(Exception e){
	//
}
}

}