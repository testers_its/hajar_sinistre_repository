package utils

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;


public class SQLConnection {


	private static Connection connection = null;
	private static String FinalResult="";

	//Establishing a connection to the DataBase
	
	def MyDatabaseConnection(String queryString){
		connectDB()
		executeQuery(queryString)
		execute(queryString)
	}
	
	

	@Keyword
	def connectDB(){
		String conn
		//Load driver class for your specific database type
		if(GlobalVariable.Global_URL.contains("dev")){
			conn= "jdbc:sqlserver://SQLNQAC2\\SQLQAC2;databaseName=ClientStatements;integratedSecurity=true"
		}
		else{
			conn = "jdbc:sqlserver://SQLNTC2\\SQLTC2;databaseName=ClientStatements;integratedSecurity=true";
		}

		if(connection != null && !connection.isClosed()){

			connection.close()

		}

		connection = DriverManager.getConnection(conn)

		return connection

	}

	/**
	 * execute a SQL query on database
	 * @param queryString SQL query string
	 * @return a reference to returned data collection, an instance of java.sql.ResultSet
	 */

	//Executing the constructed Query and Saving results in resultset

	@Keyword

	def executeQuery(String queryString) {

		Statement stm = connection.createStatement()

		ResultSet rs = stm.executeQuery(queryString)

		return rs

	}

	//Closing the connection

	@Keyword

	def closeDatabaseConnection() {

		if(connection != null && !connection.isClosed()){

			connection.close()

		}

		connection = null

	}

	/**
	 * Execute non-query (usually INSERT/UPDATE/DELETE/COUNT/SUM...) on database
	 * @param queryString a SQL statement
	 * @return single value result of SQL statement
	 */

	@Keyword

	def execute(String queryString) {

		Statement stm = connection.createStatement()

		boolean result = stm.execute(queryString)

		return result

	}


	@Keyword
	def GetResult(ResultSet rs) {

		Statement stm = connection.createStatement()

		String FinalResult=""
		ResultSetMetaData rsmd = rs.getMetaData()
		int columnCount = rsmd.getColumnCount()

		while (rs.next()) {
			for (int i = 1; i <= columnCount; i++) {
				FinalResult += (rs.getString(rsmd.getColumnName(i)) + ',') // Do stuff with name
			}
		}

		//System.out.println("Before str"+str);
		if (((FinalResult != null) && (FinalResult.length() > 0)) && (FinalResult.charAt(FinalResult.length() - 1) == ',')) {
			FinalResult = FinalResult.substring(0, FinalResult.length() - 1)

			System.out.println('****FinalResult*****' + FinalResult)
		}



		return FinalResult

	}

}
