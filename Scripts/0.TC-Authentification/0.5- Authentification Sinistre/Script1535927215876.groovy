import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.TearDownIfError as TearDownIfError
import com.kms.katalon.core.annotation.TearDownIfFailed as TearDownIfFailed
import com.kms.katalon.core.annotation.TearDownIfPassed as TearDownIfPassed
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.URL_APPLICATION)

WebUI.setText(findTestObject('Object_Declaration/txtLogin'), findTestData('0.DF-Authentification/0.1-TD Authentification').getValue(
        2, 2))

WebUI.setText(findTestObject('Object_Declaration/txtMdp'), findTestData('0.DF-Authentification/0.1-TD Authentification').getValue(
        3, 2))

WebUI.click(findTestObject('Object_Declaration/btnConnexion'))

WebUI.waitForPageLoad(GlobalVariable.DELAY_PAGE_LOAD_MID)

def Config() {
    'Configuration initiale'
    WebUI.openBrowser('')

    WebUI.maximizeWindow()
}

@TearDownIfError
def TDIfError() {
    'TearDown en cas derreur'
    WebUI.takeScreenshot()
}

@TearDownIfPassed
def TDIfPassed() {
    'TearDown en cas de succ�s'
    String path = WebUI.takeScreenshot()

    WebUI.takeScreenshot('/Users/dell/git/Captures_Resultat_Tests/test_DeclarerSinistre.png')
}

@TearDownIfFailed
def TDIfFailed() {
    'Tear down en cas dechec'
    WebUI.takeScreenshot()
}

