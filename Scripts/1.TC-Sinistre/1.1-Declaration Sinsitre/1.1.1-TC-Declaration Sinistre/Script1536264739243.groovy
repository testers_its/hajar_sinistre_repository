import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.TearDownIfError as TearDownIfError
import com.kms.katalon.core.annotation.TearDownIfFailed as TearDownIfFailed
import com.kms.katalon.core.annotation.TearDownIfPassed as TearDownIfPassed
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('0.TC-Authentification/0.1- Authentification Back'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(findTestData('Referentiel/Referentiel').getValue(2, 9))

'Unique Point dentré pour chaque cas de test et contrat'
WebUI.setText(findTestObject('Object_Declaration/Page_1535804059301/NumContrat'), findTestData('EntryData/EntryData').getValue(
        1, 1))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.click(findTestObject('Object_Declaration/Page_1536013662833/rechercheContrat'))

WebUI.waitForElementPresent(findTestObject('Object_Declaration/Page_1536013662833/selectionnerContrat'), GlobalVariable.DELAY_PAGE_LOAD_MID)

WebUI.click(findTestObject('Object_Declaration/Page_1536013662833/selectionnerContrat'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.waitForElementVisible(findTestObject('Object_Declaration/AffichageContrat'), GlobalVariable.DELAY_PAGE_LOAD_MID)

WebUI.click(findTestObject('Object_Declaration/btnDeclarerSinistre'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 2))

WebUI.waitForElementPresent(findTestObject('Object_Declaration/InterfaceDeclareSinistre'), GlobalVariable.DELAY_PAGE_LOAD_MAX)

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.click(findTestObject('Object_Declaration/Page_1533426081924/calendrierDareDeclaration'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object_Declaration/Page_1533426081924/dateToday_DateDeclaration'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.click(findTestObject('Object_Declaration/Page_1533426081924/calendrier_DateSinistre'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object_Declaration/Page_1533426081924/dateToday_DateSinistre'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object_Declaration/txtDescSinistre'), findTestData('1.DF-Sinistre/1.1-DF-Declaration Sinistre').getValue(
        3, 1))

WebUI.setText(findTestObject('Object_Declaration/txtDeclarant'), findTestData('1.DF-Sinistre/1.1-DF-Declaration Sinistre').getValue(
        4, 1))

WebUI.check(findTestObject('Object_Declaration/chckpiece1'))

WebUI.check(findTestObject('Object_Declaration/chckpiece2'))

WebUI.check(findTestObject('Object_Declaration/chckpiece3'))

WebUI.check(findTestObject('Object_Declaration/chckpiece4'))

WebUI.check(findTestObject('Object_Declaration/chckpiece5'))

WebUI.check(findTestObject('Object_Declaration/chckpiece6'))

WebUI.check(findTestObject('Object_Declaration/chckpiece7'))

WebUI.check(findTestObject('Object_Declaration/chckpiece8'))

WebUI.check(findTestObject('Object_Declaration/chckpiece9'))

WebUI.check(findTestObject('Object_Declaration/chckpiece10'))

WebUI.check(findTestObject('Object_Declaration/chckpiece11'))

WebUI.check(findTestObject('Object_Declaration/chckpiece12'))

WebUI.check(findTestObject('Object_Declaration/chckpiece13'))

WebUI.check(findTestObject('Object_Declaration/chckpiece14'))

WebUI.check(findTestObject('Object_Declaration/chckpiece15'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 3), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object_Declaration/Page_1533427580841/btnEnregistrerDecSinistre'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 3))

def statut = WebUI.verifyElementPresent(findTestObject('Object_Declaration/verifstatut'), 'Sinistré')

if (statut){
   KeywordUtil.markPassed("Declaration sinistre ok")
}


def Config() {
    'Configuration initiale'
    WebUI.openBrowser('')

    WebUI.maximizeWindow()
}

@TearDownIfError
def TDIfError() {
    'TearDown en cas derreur'
    WebUI.takeScreenshot()
}

@TearDownIfPassed
def TDIfPassed() {
    'TearDown en cas de succès'
    String path = WebUI.takeScreenshot()

    WebUI.takeScreenshot('/Users/dell/git/Captures_Resultat_Tests/test_DeclarerSinistre.png')

    WebUI.closeBrowser()
}

@TearDownIfFailed
def TDIfFailed() {
    'Tear down en cas dechec'
    WebUI.takeScreenshot()

    WebUI.closeBrowser()
}

