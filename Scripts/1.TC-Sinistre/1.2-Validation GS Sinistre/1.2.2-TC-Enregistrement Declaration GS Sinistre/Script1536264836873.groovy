import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.TearDownIfError as TearDownIfError
import com.kms.katalon.core.annotation.TearDownIfFailed as TearDownIfFailed
import com.kms.katalon.core.annotation.TearDownIfPassed as TearDownIfPassed
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('0.TC-Authentification/0.2- Authentification GS'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object_Validation_GS1/btnSelectct'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 2))

WebUI.setText(findTestObject('Object_Validation_GS1/reserveInit'), findTestData('1.DF-Sinistre/1.2-DF-Validation GS').getValue(
        10, 1))

WebUI.click(findTestObject('Object_Validation_GS1/InfoBeneficiaire/OngletInfoBenef'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 2))

WebUI.setText(findTestObject('Object_Validation_GS1/InfoBeneficiaire/IdentifiantBenef'), findTestData('1.DF-Sinistre/1.2-DF-Validation GS').getValue(
        3, 1))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 2))

WebUI.setText(findTestObject('Object_Validation_GS1/InfoBeneficiaire/Nom_Prenom_Benef'), findTestData('1.DF-Sinistre/1.2-DF-Validation GS').getValue(
        4, 1))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.setText(findTestObject('Object_Validation_GS1/InfoBeneficiaire/PourcentageBenef'), findTestData('1.DF-Sinistre/1.2-DF-Validation GS').getValue(
        5, 1))

WebUI.selectOptionByLabel(findTestObject('Object_Validation_GS1/InfoBeneficiaire/TypeBenef'), findTestData('1.DF-Sinistre/1.2-DF-Validation GS').getValue(
        6, 1), true)

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 3))

WebUI.selectOptionByLabel(findTestObject('Object_Validation_GS1/InfoBeneficiaire/ModeReglement'), findTestData('1.DF-Sinistre/1.2-DF-Validation GS').getValue(
        7, 1), false)

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 2))

WebUI.setText(findTestObject('Object_Validation_GS1/InfoBeneficiaire/RIBBenef'), findTestData('1.DF-Sinistre/1.2-DF-Validation GS').getValue(
        8, 1))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 3))

WebUI.click(findTestObject('Object_Validation_GS1/InfoBeneficiaire/btnAjouterBenef'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 3))

WebUI.click(findTestObject('Object_Validation_GS1/Page_1536265197567/btnEnregistrer'))

WebUI.acceptAlert(FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Object_Validation_GS1/CodeEnregistrer'), 'ENREGISTRER')

def Config() {
    'Configuration initiale'
    WebUI.openBrowser('')

    WebUI.maximizeWindow()
}

@TearDownIfError
def TDIfError() {
    'TearDown en cas derreur'
    WebUI.takeScreenshot()
}

@TearDownIfPassed
def TDIfPassed() {
    'TearDown en cas de succès'
    String path = WebUI.takeScreenshot()

    WebUI.takeScreenshot('C:/STAS/Client/Stas/hajar_sinistre_repository/Captures_Resultat_Tests/EnregistrementGS1.png')
}

@TearDownIfFailed
def TDIfFailed() {
    'Tear down en cas dechec'
    WebUI.takeScreenshot()
}

