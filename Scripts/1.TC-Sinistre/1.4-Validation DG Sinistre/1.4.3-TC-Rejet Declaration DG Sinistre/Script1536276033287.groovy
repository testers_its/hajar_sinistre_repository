import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.TearDownIfError as TearDownIfError
import com.kms.katalon.core.annotation.TearDownIfFailed as TearDownIfFailed
import com.kms.katalon.core.annotation.TearDownIfPassed as TearDownIfPassed
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('0.TC-Authentification/0.4- Authentification DG'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Rep_DirecteurSinistre/btnSelectDS'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.selectOptionByLabel(findTestObject('motif'), 'AUTRES', false)

WebUI.click(findTestObject('passerSansSuite'))

WebUI.acceptAlert(FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextNotPresent('VALIDER', false, FailureHandling.STOP_ON_FAILURE)

def Config() {
    'Configuration initiale'
    WebUI.openBrowser('')

    WebUI.maximizeWindow()
}

@TearDownIfError
def TDIfError() {
    'TearDown en cas derreur'
    WebUI.takeScreenshot()
}

@TearDownIfPassed
def TDIfPassed() {
    'TearDown en cas de succès'
    String path = WebUI.takeScreenshot()

    WebUI.takeScreenshot('C:/STAS/Client/Stas/hajar_sinistre_repository/Captures_Resultat_Tests/RejetDG.png')
}

@TearDownIfFailed
def TDIfFailed() {
    'Tear down en cas dechec'
    WebUI.takeScreenshot()
}

