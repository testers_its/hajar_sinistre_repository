import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.TearDownIfError as TearDownIfError
import com.kms.katalon.core.annotation.TearDownIfFailed as TearDownIfFailed
import com.kms.katalon.core.annotation.TearDownIfPassed as TearDownIfPassed
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('0.TC-Authentification/0.5- Authentification Sinistre'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(findTestData('Referentiel/Referentiel').getValue(2, 10))

WebUI.waitForPageLoad(GlobalVariable.DELAY_PAGE_LOAD_MIN)

WebUI.click(findTestObject('Repository_Decaissement/rechercheDecaissement'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.click(findTestObject('calendriersls_xo'))

WebUI.click(findTestObject('dateAuj'))

WebUI.click(findTestObject('chercher'))

WebUI.click(findTestObject('reglersinistre'))

WebUI.click(findTestObject('Repository_Decaissement/AjouterSinistre'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.setText(findTestObject('contratset'), findTestData('EntryData/EntryData').getValue(2, 3))

WebUI.click(findTestObject('Repository_Decaissement/RechercherContrat'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.verifyElementNotPresent(findTestObject('cocher'), GlobalVariable.DELAY_PAGE_LOAD_MIN)

WebUI.click(findTestObject('Repository_Decaissement/AjouterContratSinistre'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.click(findTestObject('Repository_Decaissement/ValiderDecaiss'))

WebUI.delay(findTestData('Referentiel/Referentiel').getValue(2, 1))

WebUI.verifyTextPresent('La liste des sinistres est vide !', false)

def Config() {
    'Configuration initiale'
    WebUI.openBrowser('')

    WebUI.maximizeWindow()
}

@TearDownIfError
def TDIfError() {
    'TearDown en cas derreur'
    WebUI.takeScreenshot()
}

@TearDownIfPassed
def TDIfPassed() {
    'TearDown en cas de succès'
    String path = WebUI.takeScreenshot()

    WebUI.takeScreenshot('/Users/dell/git/Captures_Resultat_Tests/Decaissement.png')
}

@TearDownIfFailed
def TDIfFailed() {
    'Tear down en cas dechec'
    WebUI.takeScreenshot()
}

