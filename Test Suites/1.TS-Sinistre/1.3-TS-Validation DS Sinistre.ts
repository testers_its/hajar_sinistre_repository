<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1.3-TS-Validation DS Sinistre</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-04T00:09:53</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>79491e48-6ce3-4723-bb84-67de8037bbb7</testSuiteGuid>
   <testCaseLink>
      <guid>00764c39-cf4f-4f2a-b1ee-cac42f7d079c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.1-Declaration Sinsitre/1.1.1-TC-Declaration Sinistre</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2107280-5a7d-4017-82cd-b62b17e17315</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.2-Validation GS Sinistre/1.2.1-TC-Validation Declaration GS Sinistre</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9c881af-bade-465d-8573-3da4932168fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.3-Validation DS Sinistre/1.3.1-TC-Validation DS Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3757b157-d6b7-4dab-9dfe-639901402a7e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0ea7cfe4-fdd1-41db-8eb6-c50535af804e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b01495b8-28a8-45d5-906a-340e85e38523</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
