<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1.4-TS-Validation DG Sinistre</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-04T00:13:39</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5e0b9baa-f0b7-4fd5-b930-9c8779f04927</testSuiteGuid>
   <testCaseLink>
      <guid>69047d16-7393-4d5c-b621-01ee026107e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.1-Declaration Sinsitre/1.1.1-TC-Declaration Sinistre</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e487073-986b-4004-9c0f-d49f78a9880f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.2-Validation GS Sinistre/1.2.1-TC-Validation Declaration GS Sinistre</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1cf3f2c-a312-4226-9915-f06db5591330</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.3-Validation DS Sinistre/1.3.1-TC-Validation DS Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3757b157-d6b7-4dab-9dfe-639901402a7e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0ea7cfe4-fdd1-41db-8eb6-c50535af804e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b01495b8-28a8-45d5-906a-340e85e38523</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6d927664-6b4d-4b11-b6c1-72bc82548775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.4-Validation DG Sinistre/1.4.1-TC-Validation DG Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>461a5132-d58a-4841-a144-317538cbf506</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f19a95dc-fe73-4d08-85e4-121be75a3099</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>795ba87b-a4aa-46a8-aac4-c3d4b5feb670</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
