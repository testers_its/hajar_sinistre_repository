<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>3.2-TS-Module Sinistre</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-04T00:58:38</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>27228606-b3e7-4232-a721-ea1a262ee204</testSuiteGuid>
   <testCaseLink>
      <guid>94b44dec-de4a-400f-b731-47e406538d88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.1-Declaration Sinsitre/1.1.1-TC-Declaration Sinistre</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13f4527c-8a3c-4f60-a4bb-923b58afc1fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.2-Validation GS Sinistre/1.2.1-TC-Validation Declaration GS Sinistre</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26e07a4e-8941-4eaf-b4a9-148eadd74d64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.3-Validation DS Sinistre/1.3.1-TC-Validation DS Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3757b157-d6b7-4dab-9dfe-639901402a7e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0ea7cfe4-fdd1-41db-8eb6-c50535af804e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b01495b8-28a8-45d5-906a-340e85e38523</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>51308ccf-5214-4fd1-b970-6a2d193e8370</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.4-Validation DG Sinistre/1.4.1-TC-Validation DG Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>461a5132-d58a-4841-a144-317538cbf506</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f19a95dc-fe73-4d08-85e4-121be75a3099</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>795ba87b-a4aa-46a8-aac4-c3d4b5feb670</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e4eaf923-ccaa-4a79-a237-524220e397f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.5-Indemnisation Sinistre/1.5.1-TC-Indemnistation Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7f3d15ed-a165-4470-b404-3ecbbbd31ee7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0ef93693-bba0-4d6d-b287-356259df3832</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6547b56c-828b-4d31-963e-6aa8c9464db5</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
