<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>4.1-TS-E2E</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-04T01:21:08</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fe7e62a5-d0da-4165-9680-0312b0931fce</testSuiteGuid>
   <testCaseLink>
      <guid>599786b5-b2c1-45c4-ad55-d80cb618ca2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.1-Declaration Sinsitre/1.1.1-TC-Declaration Sinistre</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>165d7242-9626-403d-9d11-8d8fafec8790</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.2-Validation GS Sinistre/1.2.1-TC-Validation Declaration GS Sinistre</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad42d1fa-12e9-4659-a87f-2ae0b84c75e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.3-Validation DS Sinistre/1.3.1-TC-Validation DS Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3757b157-d6b7-4dab-9dfe-639901402a7e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0ea7cfe4-fdd1-41db-8eb6-c50535af804e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b01495b8-28a8-45d5-906a-340e85e38523</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>17aaf91d-b79f-400d-b8cf-56895860c5f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.4-Validation DG Sinistre/1.4.1-TC-Validation DG Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>461a5132-d58a-4841-a144-317538cbf506</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f19a95dc-fe73-4d08-85e4-121be75a3099</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>795ba87b-a4aa-46a8-aac4-c3d4b5feb670</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1ca0d54c-e55d-4636-a80d-a33c13a20e30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.TC-Sinistre/1.5-Indemnisation Sinistre/1.5.1-TC-Indemnistation Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7f3d15ed-a165-4470-b404-3ecbbbd31ee7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0ef93693-bba0-4d6d-b287-356259df3832</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6547b56c-828b-4d31-963e-6aa8c9464db5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2e318dae-ff02-44a3-abc9-0a7704cbf995</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.TC-Decaissement/2.1-TC-Decaissement Contrat Sinistre</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c5cd96a0-c231-492a-8bc4-119c6c5c966b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>babbea74-0dc0-46bb-a65a-13db447a2d25</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>972bfaaa-3ca9-4749-906b-f81827923482</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>577ab2e1-d5a5-498f-bb40-f485eeb78d18</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
